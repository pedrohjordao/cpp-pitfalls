---
theme: gaia
_class: lead
paginate: true
backgroundColor: #ffff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
footer: October 2020
marp: true
---

![w:400](assets/cpp.png)

# **C++ Pitfalls and How to Avoid Them**

Pedro Jordão

---

# Presenter

Pedro Jordão

* Software Developer for 6 years
* C++, Rust and Fortran (:weary:) for fast simulation code
* JVM based languages for backend and GUI
* Currently working with the Foresight team @criticaltechworks with Scala, Apache Flink and Apache Spark

---

# About This Presentation

* The original version was presented as "Rust for C++ Developers"
* Scraped most of the Rust content to focus on C++ and how to bridge the gap from other languages (Rust, Java)
* This is a somewhat interactive presentation, there will be questions, so try to participate!

---

# About This Presentation

* Please mute your microphones and use the "Raise Hand" feature of Teams!
    * Both to ask questions and participate in the interactive parts!
* I hope you Enjoy it

---

# What This Talk Is

* A show of quirks of C++ 
* A show of how to avoid them before running your code
* A basic overview of tooling to help you write better code
* How other languages avoid these pitfalls

---

# What This Talk Is Not

* A C++ tutorial
* A discussion about runtime tools (maybe next time)
* A Full language review (I only have 30 minutes!)
* Diminishing C++ importance
    * I love C++!

---

# Quick C++ Intro

* Designed in 1979
* Low Level
* Natively Compiled  
* Statically Typed
* Manual Memory Management 
* As Fast as C*

---

# Let's Play a Game

```cpp
// What is wrong with this code
void fun() {
    int* i;
    {
        int j = 0;
        i = &j;
    }
    std::cout << *i << '\n';
}
```
https://godbolt.org/z/KpzaZd

---

# Let's Play a Game

```cpp
// what is wrong here?
void fun() {
    vector<int> v;
    v.push_back(42);
    auto& i = v[0];
    v.push_back(666);
    std::cout << i << '\n';
}
```
https://godbolt.org/z/GtZxCY

---

# How Java Handles It

```java
public void fun() {
    var v = new ArrayList<Integer>();
    v.push(42);
    var i = v.get(0);
    v.push(666);
    System.out.println(i);
}
```

---

# How Java Handles It

![center h:480](assets/java1.svg)

--- 
# How Java Handles It

![center h:480](assets/java2.svg)

--- 

# Let's Play a Game

```cpp
class Account {
public:
    double balance = 10'000'000.0;

    bool withdraw(double amount) {
        double new_balance = balance - amount;
        if (new_balance < 0.0) {
            return false;
        } else {
            balance = new_balance;
            return true;
        }
    }
};
```

---

# Let's Play a Game

```cpp
// what is wrong here?
void fun() {
    // main thread
    auto shared_account = make_shared<Account>();
    // ...
    // In many different threads
    shared_account->withdraw(100.0)
}
```
https://godbolt.org/z/ctJ6Zt

---

# Bonus Question

```cpp
// what is wrong here?
void fun() {
    // Thread I
    shared_ptr<A> a{new A(1)};
    // Thread II
    shared_ptr<A> b{a};
    // Thread III
    shared_ptr<A> c{a};
    // Thread IV
    shared_ptr<A> d{a};
    d.reset(new A(10));
    // what is the value of a, b and c at this point?
}
```

---

# Let's Play a Game

Stolen from https://www.youtube.com/watch?v=3MB2iiCkGxg

* Is `shared_ptr` thread safe?
    * Yes?
    * No?
    * It depends?
    * Just as safe as a normal pointer?

---

# Let's Play a Game

![center h:500](assets/shared_ptr_google.png)

---
# Let's Play a Game

![center h:480](assets/shared_ptr_structure.png)

---

# How Rust Handles It

```rust
struct Account {
    balance: f32
}
impl Account {
    fn new() -> Account {
        Account {
            balance: 10_000_000.0
        }
    }
    //...
```

---

# How Rust Handles It

```rust
    fn withdraw(&mut self, amount: f32) -> bool {
        let new_balance = self.balance - amount;
        if new_balance < 0.0 {
            false
        } else {
            self.balance = new_balance;
            true
        }
    }
}
```

---

# How Rust Handles It

```rust
fn main() {
    let shared_account = Arc::new(Account::new());
    let shared_account1 = shared_account.clone();
    let shared_account2 = shared_account.clone();
    thread::spawn(move || 
        while shared_account1.withdraw(100.0) {}
    );
    thread::spawn(move || 
        while shared_account2.withdraw(100.0) {}
    );
}
```
https://godbolt.org/z/ykJ4Sx

---

# Fixing it

```rust
fn main() {
    let shared_account = Arc::new(RwLock::new(Account::new()));
    let shared_account1 = shared_account.clone();
    let shared_account2 = shared_account.clone();
    thread::spawn(move || 
        while shared_account1.write().unwrap().withdraw(100.0) {}
    );
    thread::spawn(move || 
        while shared_account2.write().unwrap().withdraw(100.0) {}
    );
}
```
https://godbolt.org/z/bJ3w-N

---

# Back to C++

* How can we avoid these pitfalls? 
  * Compiler flags
  * Tooling (Static Analysis)
  * Experimental compiler features

---

# Compiler Flags

* Enabling warnings through `-Wall` is the least you can do
  * Ironically `all` is not really "all"
    * Yay consistency
  * Also enable `-Wextra` for completeness
    * https://godbolt.org/z/TG7qE1 
  * PS: `-Wextra` is still not all, but is a nice baseline

---

# Compiler Flags

* But be careful with UB!
    * On some situations you might not get any warnings (especially on GCC)
      * https://godbolt.org/z/EEM9cf
    * Ideally you should have the code compiled without optimizations as a step in your CICD pipeline (if you are only relying on these warnings)
---

# Tooling

* There are some great static analysis tools out there
    * PVS-Studio
    * Clang-Tidy
* Whenever possible, the usage of the C++ Core Guidelines 

--- 

# Tooling

* C++ Core Guidelines are a general set of recommendations 
on how to write C++ code
    * https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
    * Tools like Clang-Tidy and MS Visual Studio implements checks for the core guidelines

---

# Tooling

* There is also a reference implementation by MS of a support library that facilitates writing code following the guidelines
    * https://github.com/microsoft/GSL
* Some of the work from this library and guidelines in general is slowly getting added to the `std`, like `std:span` on C++20

---

# Tooling

* PVS-Studio is a great, but costly tool for C++ (and C, and Java!) Static Analysis
    * If you don't know it, head over to their page. 
        * https://www.viva64.com/en/pvs-studio/
    * They produce amazing blog posts with the results of analysis of open source code bases

--- 

# Tooling

* A free, open source and pretty much as good alternative is Clang-Tidy
* Has checks for pretty much everything, but can be a pain to set up correctly
* Has core guidelines checks
* You can write your own checks

--- 

# Tooling

* Let's go back to our previous examples and check the results with these tools
    * https://godbolt.org/z/KpzaZd
    * https://godbolt.org/z/GtZxCY
    * https://godbolt.org/z/ctJ6Zt

---

# Tooling

* If you are in a position of starting a project form scratch I highly recommend Jason Turner's C++ project starter
    * https://github.com/lefticus/cpp_starter_project
* It integrates general best practices and tooling 

---

# Experimental Compiler Features

* If you can set up an experimental compiler as part of your CICD pipeline, then there are some interesting tooling available    
* The main example Clang's `-Wlifetime` flag
* This brings the C++ compiler closer to Rust's compiler pointer
lifetime tracking features

---

# Experimental Compiler Features

```cpp
void fun() {
    vector<int> v;
    v.push_back(42);
    auto& i = v[0];
    v.push_back(666);
    std::cout << i << '\n';
}
```
https://godbolt.org/z/Ee6vx4

---

# Experimental Compiler Features

```cpp
void fun() {
    vector<int> v;
    v.push_back(42);
    auto& i = v[0];
    v.push_back(666);
    std::cout << i << '\n';
}
```
https://godbolt.org/z/Jcytvk


---

# What About the Accounts Example?

* As we've seen, no tool was able to help us with the over-withdraw from accounts example
* Sadly, there's not much that can be done there without a considerable overhaul of the language
* In all fairness, very few languages are able to handle at compile time these cases
    * Rust because of its very strict type system
    * Haskell because of its immutability

---

# What About the Accounts Example?

* So what can you do?
    * If something is shared between threads, make it immutable and you are (probably) safe
    * If it can't be immutable think very carefully about what is going on and what guarantees you need to provide
    * When in doubt you probably want to synchronize your data with a `std::mutex` or `std::atomic<shared_ptr<T>>` (C++20)

---

# Conclusion

* There is no magic bullet to write perfect C++ code
* Use tooling and integrate it on your and your team's workflow 
    * Take it seriously! Do not ignore warnings, use `-Werror` in your CICD pipeline
* But more importantly...
---

# Conclusion

* Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!
    * Unit test!

# Conclusion

* Write testable code
    * No, using macros to substitute parts of your code when in testing is not testable code
    * Writing code ready with dependency injection in mind, be it through pure virtual classes, 
    higher order functions, builders, functor patterns or whatever.
* Use basic SOLID principles 
    * Write pure functions whenever possible


---

# Conclusion

* Join us on Yammer and Teams @Rust CoP
* Q&A